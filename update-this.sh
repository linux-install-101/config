echo "Pulling current .zshrc into repo"
rm .zshrc.bak
mv .zshrc .zshrc.bak
cp ~/.zshrc .zshrc
echo "Pulling current pacwall config into repo"
rm -r pacwall.bak
mv pacwall pacwall.bak
cp -r ~/.config/pacwall ./
echo "Pushing to remote"
git add .
git commit -m "Auto Config Update"
git push
echo "Pushing complete"
