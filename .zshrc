# Use powerline
 USE_POWERLINE="true"
# Source manjaro-zsh-configuration
 if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi

# Default Applications
export VISUAL=vim
export EDITOR="$VISUAL"

# Directory Stuff
alias la='ls -a'
alias l='ls -lh'

# Packages
alias update='paru -Syyu --noconfirm && pacwall'
alias remove='function _(){paru -Rns $1 && pacwall}; _'
alias install='function _(){paru -S $1 && pacwall}; _'
alias clense='paru -Qtdq | paru -Rns - && pacwall'
alias search='function _(){paru -Ss $1 | less}; _'
alias query='paru -Q'

# Git
alias pull='git pull'
alias status='git status'
alias commit='git commit'
alias push='git push'
alias add='git add'
alias clone='git clone'

tmux && exit
source ~/.profile
clear
pfetch
